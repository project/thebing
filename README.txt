Thebing-Snippet

The plugin allows you to link all online forms of the Thebing Management Software easily with your Drupal website.

Usable placeholders to display the forms for the Thebing School Management Software:

Placement test:
[thebing:placement_test:url-to-school-software:A:B]

Registration form:
[thebing:registrationform:url-to-school-software:1=KEY-3720362838:de]

Default:
[thebing:default:url-to-school-software:GURLGP67RMWSKFQ3:72CRNEML5QZAYZZ8]

The server, combination key, template key and ID of the above displayed placeholders needs to be replaced with your actual data.